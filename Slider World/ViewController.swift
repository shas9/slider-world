//
//  ViewController.swift
//  Slider World
//
//  Created by Kite Games Studio on 5/9/21.
//

import UIKit


class ViewController: UIViewController {

    @IBOutlet weak var viewToPerform: UIView!
    @IBOutlet weak var bottomView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        addBottomPanel()   // should be called if the instance is called programatically
    }

    func addBottomPanel() {
        let frame = self.bottomView.bounds
        let bottomPanel = BottomPanelView(frame: frame)
        bottomPanel.delegate = self
        self.bottomView.addSubview(bottomPanel)
    }


}

extension ViewController: BottomPanelViewDelegate {

    func updateFrame() {
        let nowFrame = self.viewToPerform.frame
        print(nowFrame)

//        self.viewToPerform.bounds = nowFrame

    }

    func doRotate(angle: Float) {
        let angleByRadian = (.pi / 180) * angle
        self.viewToPerform.transform = CGAffineTransform(rotationAngle: CGFloat(angleByRadian))

//        self.viewToPerform.transform = viewToPerform.transform.rotated(by: CGFloat(angleByRadian))
        updateFrame() 
    }

    func doScale(x: CGFloat, y: CGFloat) {
        self.viewToPerform.transform = CGAffineTransform(scaleX: x, y: y)

        updateFrame()
    }

    func doMove(x: CGFloat) {

        if x <= 150 {
            self.viewToPerform.transform = CGAffineTransform(translationX: x - 150, y: 0)
        } else {
            let newX = x - 150

            self.viewToPerform.transform = CGAffineTransform(translationX: newX, y: 0)
        }

//        self.viewToPerform.frame.origin.x = x

        updateFrame()
    }

    func doShift() {
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)

        if let secondViewController = storyBoard.instantiateViewController(identifier: "ViewController2") as? ViewController2 {
//            blueVC.blueVCDelegate = self
            self.present(secondViewController, animated: true, completion: nil)
        }
    }
}


