//
//  BottomPanelView.swift
//  Slider World
//
//  Created by Kite Games Studio on 5/9/21.
//

import UIKit

protocol BottomPanelViewDelegate: NSObject {
    func doRotate(angle: Float)
    func doScale(x: CGFloat, y: CGFloat)
    func doMove(x: CGFloat)
    func doShift()
}

class BottomPanelView: UIView {

    weak var delegate: BottomPanelViewDelegate!

    @IBOutlet weak var sliderPanel: UISlider!
    @IBOutlet weak var labelToSlider: UILabel!

    var isSliderPanelHidden: Bool = false
    var isRotationOpened: Bool = false
    var isScaleOpened: Bool = false
    var isMoveOpened: Bool = false

    // Mark:- Must to present when creating a xib file
    override init(frame: CGRect) {
        super.init(frame: frame)
        let view = loadFromNib()
        view.frame = frame
        self.initalSetting()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        let view = loadFromNib()
        view.frame = self.bounds
        self.initalSetting()
    }

    func loadFromNib() -> UIView {
        let bundleName = Bundle(for:type(of: self))
        let nibName = String(describing: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundleName)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        self.addSubview(view)
        return view
    }

    private func initalSetting() {
        self.hideSliderPanel()
        self.sliderPanel.minimumTrackTintColor = UIColor.black
        self.sliderPanel.maximumTrackTintColor = UIColor.brown
    }
    // End here


    func hideSliderPanel() {

        guard isSliderPanelHidden == false else { return }

        self.labelToSlider.text = "Bye!"

        UIView.animate(withDuration: 0.6, delay: 0.5, usingSpringWithDamping: 0.6, initialSpringVelocity: 10, animations: {
            self.sliderPanel.frame.origin.y += 50
            self.sliderPanel.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
            self.sliderPanel.alpha = 0
        }, completion: { _ in

        })

        UIView.animate(withDuration: 0.6, delay: 0.5, usingSpringWithDamping: 0.6, initialSpringVelocity: 10, animations: {
            self.labelToSlider.frame.origin.y += 50
            self.labelToSlider.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
            self.labelToSlider.alpha = 0
        }, completion: { _ in

        })

        isSliderPanelHidden = true
    }

    func showSliderPanel() {

        if !isSliderPanelHidden {
            hideSliderPanel()
        }

        self.labelToSlider.text = "Slide and Enjoy"

        UIView.animate(withDuration: 0.6, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 10, animations: {
            self.sliderPanel.frame.origin.y -= 50
            self.sliderPanel.transform = CGAffineTransform(scaleX: 1, y: 1)
            self.sliderPanel.alpha = 1
        }, completion: { _ in

        })

        UIView.animate(withDuration: 0.6, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 10, animations: {
            self.labelToSlider.frame.origin.y -= 50
            self.labelToSlider.transform = CGAffineTransform(scaleX: 1, y: 1)
            self.labelToSlider.alpha = 1
        }, completion: { _ in

        })


        isSliderPanelHidden = false
    }

    @IBAction func sliderButtonAdjusted(_ sender: Any) {

        print("Slider Button Adjusted")

        let value = sliderPanel.value

        self.labelToSlider.text = "The value is: \(value)"

        if isRotationOpened {
            self.delegate.doRotate(angle: value)

        } else if isScaleOpened {

            self.delegate.doScale(x: CGFloat(value), y: CGFloat(value))

        } else if isMoveOpened {
            self.delegate.doMove(x: CGFloat(value))
        }
        else { fatalError("it should not be here") }
    }

    @IBAction func rotateButtonPressed(_ sender: Any) {

        print("Rotate Button Pressed")

        self.showSliderPanel()

        self.sliderPanel.maximumValue = 360
        self.sliderPanel.minimumValue = 0

        isRotationOpened = true
        isScaleOpened = false
        isMoveOpened = false

    }

    @IBAction func scaleButtonPressed(_ sender: Any) {

        print("Scaling Button Pressed")

        self.showSliderPanel()
        self.sliderPanel.maximumValue = 2
        self.sliderPanel.minimumValue = 0.5

        isRotationOpened = false
        isScaleOpened = true
        isMoveOpened = false
    }

    @IBAction func shiftButtonPressed(_ sender: Any) {

        print("Shift Button Pressed")

        self.hideSliderPanel()

        isRotationOpened = false
        isScaleOpened = false
        isMoveOpened = false

        self.delegate.doShift()
    }

    @IBAction func moveButtonPressed(_ sender: Any) {

        print("Move Button Pressed")

        self.showSliderPanel()
        self.sliderPanel.maximumValue = 300
        self.sliderPanel.minimumValue = 0

        isRotationOpened = false
        isScaleOpened = false
        isMoveOpened = true
    }

}
