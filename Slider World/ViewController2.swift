//
//  ViewController2.swift
//  Slider World
//
//  Created by Kite Games Studio on 6/9/21.
//

import UIKit

class ViewController2: UIViewController {

    @IBOutlet weak var viewToPerform: UIView!
    @IBOutlet weak var bottomView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        addBottomPanel()   // should be called if the instance is called programatically
    }

    func addBottomPanel() {

        print("Add bottom panel is called from view controller 2")
        let frame = self.bottomView.bounds
        let bottomPanel = BottomPanelView(frame: frame)
        bottomPanel.delegate = self
        self.bottomView.addSubview(bottomPanel)
    }

}

extension ViewController2: BottomPanelViewDelegate {

    func updateFrame() {
        let nowFrame = self.viewToPerform.frame
        print(nowFrame)

        //self.viewToPerform.bounds = nowFrame
    }

    func doRotate(angle: Float) {
        let angleByRadian = (.pi / 180) * angle
        self.viewToPerform.transform = CGAffineTransform(rotationAngle: CGFloat(angleByRadian))

        updateFrame()
    }

    func doScale(x: CGFloat, y: CGFloat) {
        self.viewToPerform.transform = CGAffineTransform(scaleX: x, y: y)

        updateFrame()
    }

    func doMove(x: CGFloat) {

        if x <= 150 {
            self.viewToPerform.transform = CGAffineTransform(translationX: x - 150, y: 0)
        } else {
            let newX = x - 150

            self.viewToPerform.transform = CGAffineTransform(translationX: newX, y: 0)
        }

//        self.viewToPerform.frame.origin.x = x

        updateFrame()
    }

    func doShift() {
        self.dismiss(animated: true)
    }
}
